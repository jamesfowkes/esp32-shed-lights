#ifndef _ADAFRUIT_ANIM_H_
#define _ADAFRUIT_ANIM_H_

void rainbow(Adafruit_WS2801& strip, uint16_t brightness);
void rainbow_cycle(Adafruit_WS2801& strip, uint16_t brightness);

#endif
