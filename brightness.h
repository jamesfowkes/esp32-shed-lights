#ifndef _BRIGHTNESS_H_
#define _BRIGHTNESS_H_

#define HHMM_TO_SECS(hh,mm) ((60 * 60 * hh) + (60 * mm)) 

uint16_t calculate_brightness(uint32_t now, uint32_t on_time, uint32_t off_time);

#endif
