#include <Esp.h>
#include <TaskAction.h>
#include <esp_system.h>
#include <EEPROM.h>
#include <WebOTA.h>
#include "fixed-length-accumulator.h"

#include "app-wifi.h"
#include "application.h"
#include "server.h"

const char* NTPSERVERS[] = {
    "pool.ntp.org",
    "0.pool.ntp.org",
    "1.pool.ntp.org"
};

void setup()
{
    Serial.begin(115200);
    Serial.setDebugOutput(true);
    esp_log_level_set("*", ESP_LOG_VERBOSE);

    app_wifi_setup();
    application_setup();
    server_setup();

    configTzTime(PSTR("GMT0BST-1,M3.5.0/1,M10.5.0"), NTPSERVERS[0],  NTPSERVERS[1],  NTPSERVERS[2]);

    webota.set_custom_html("<h1>Shed Lights OTA Update</h1>");
}

void loop()
{
    application_loop();
    server_loop();
    webota.handle();    
}
