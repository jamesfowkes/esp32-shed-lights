#ifndef _HTTP_HANDLER_H_
#define _HTTP_HANDLER_H_

#include <HTTPClient.h>
#include <TaskAction.h>

class HTTPHandler
{

public:

    HTTPHandler();

    void loop();
    bool start_download(char const * const url);
    bool handle_get_stream(FixedLengthAccumulator& dst);

private:

    void download_state_printer();

    HTTPClient * mp_http_client = NULL;
    int m_download_size = 0;
    uint16_t m_download_count = 0;
    bool m_done = false;
    uint8_t m_printer_next_tick = 100;
};

#endif
