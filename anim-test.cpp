#ifndef ARDUINO

#include <stdint.h>

typedef uint8_t byte;

class Adafruit_WS2801
{
public:	
	void setPixelColor(uint8_t n, uint8_t r, uint8_t g, uint8_t b);
	void setPixelColor(uint8_t n, uint32_t color);
	void show();
	int numPixels() { return 20; }
};

#endif
