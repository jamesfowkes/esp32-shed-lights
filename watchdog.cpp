/* Platform Includes */

#include <Esp.h>
#include <esp_system.h>

static hw_timer_t *timer = NULL;

void IRAM_ATTR resetModule() {
    esp_restart();
}

void watchdog_start()
{
    timer = timerBegin(0, 80, true);
    timerAttachInterrupt(timer, &resetModule, true);
    timerAlarmWrite(timer, 1000, false);
    timerAlarmEnable(timer);
}

void watchdog_kick()
{
    timerWrite(timer, 0);
}
