/* C/C++ Includes */

#include <sys/time.h>

/* Arduino Includes */

#include <TaskAction.h>
#include <esp_system.h>
#include <EEPROM.h>
#include <Adafruit_WS2801.h>

/* Application Includes */

#include "app-wifi.h"
#include "application.h"
#include "adafruit_anim.h"
#include "sun-times.h" 
#include "brightness.h" 

#define OFF_TIME HHMM_TO_SECS(22,00)

static const uint8_t LED_PIN = 2;

// Pixel is wired:
// 12V
// Clock (Blue)
// Data (Green)
// GND

// On ESP32 side, data (green) is closer to the USB connector

static const uint8_t DATA_PIN = 14;
static const uint8_t CLOCK_PIN = 27;
static const uint8_t NLEDS = 20;

static Adafruit_WS2801 s_strip = Adafruit_WS2801(20, DATA_PIN, CLOCK_PIN);
static uint16_t s_brightness = 0;

static void leds_task_fn(TaskAction* this_task)
{
    if (s_brightness)
    {
        rainbow_cycle(s_strip, s_brightness);
    }
    else
    {
        for (uint8_t i=0; i<NLEDS; i++)
        {
            s_strip.setPixelColor(i, 0, 0, 0);
        }
        s_strip.show();
    }
}

static TaskAction s_leds_task(leds_task_fn, 1000, INFINITE_TICKS);

static void heartbeat_task_fn(TaskAction* this_task)
{
    (void)this_task;
    static bool led_on = false;

    led_on = !led_on;
    digitalWrite(LED_PIN, led_on);
}
static TaskAction s_heartbeat_task(heartbeat_task_fn, 500, INFINITE_TICKS);

static void debug_task_fn(TaskAction* this_task)
{
    if (s_brightness)
    {
        Serial.print("Brightness =");
        Serial.print(s_brightness);
        Serial.println("/255");
    }
}
static TaskAction s_debug_task(debug_task_fn, 5000, INFINITE_TICKS);

static void brightness_update_task_fn(TaskAction* this_task)
{
    uint32_t sunset_time_seconds = suntime_get_sunset_timestamp();
    if (sunset_time_seconds)
    {
        struct tm time;
        if (getLocalTime(&time, 0))
        {
            time_t now = (time.tm_hour * 3600) + (time.tm_min * 60) +  time.tm_sec;
            s_brightness = calculate_brightness(now, sunset_time_seconds, OFF_TIME);
        }
    }
}
static TaskAction s_brightness_update_task(brightness_update_task_fn, 1000, INFINITE_TICKS);

uint8_t application_get_brightness()
{
    return s_brightness;
}

void application_setup()
{
    pinMode(LED_PIN, OUTPUT);
    pinMode(CLOCK_PIN, OUTPUT);
    pinMode(DATA_PIN, OUTPUT);
    s_strip.begin();
}

void application_loop()
{
    suntime_update();

    s_debug_task.tick();
    s_brightness_update_task.tick();
    s_leds_task.tick();
    s_heartbeat_task.tick();
    
}
