//#define TEST

#ifndef TEST
#include <Adafruit_WS2801.h>
#else
#include "anim-test.cpp"
#endif

// Create a 24 bit color value from R,G,B
static uint32_t MakeColor(byte r, byte g, byte b, byte brightness)
{
    uint32_t c;
    c = ((uint16_t)r * brightness) / 255;
    c <<= 8;
    c |= ((uint16_t)g * brightness) / 255;
    c <<= 8;
    c |= ((uint16_t)b * brightness) / 255;
    return c;
}

//Input a value 0 to 255 to get a color value.
//The colours are a transition r - g -b - back to r
static uint32_t Wheel(byte WheelPos, byte brightness)
{
    if (WheelPos < 85) {
        return MakeColor(WheelPos * 3, 255 - WheelPos * 3, 0, brightness);
    } else if (WheelPos < 170) {
        WheelPos -= 85;
        return MakeColor(255 - WheelPos * 3, 0, WheelPos * 3, brightness);
    } else {
        WheelPos -= 170; 
        return MakeColor(0, WheelPos * 3, 255 - WheelPos * 3, brightness);
    }
}

/* Public Functions */

void rainbow(Adafruit_WS2801& strip, uint16_t brightness)
{
    static int i=0;
    static int j=0;

    if (j < 256) { j++; } else {j = 0;}
    if (i < strip.numPixels()) { i++; } else {i = 0;}

    strip.setPixelColor(i, Wheel( (i + j) % 255, brightness));
    strip.show();
}

// Slightly different, this one makes the rainbow wheel equally distributed 
// along the chain
void rainbow_cycle(Adafruit_WS2801& strip, uint16_t brightness)
{

    static int i=0;
    static int j=0;
    
    for (i=0; i < strip.numPixels(); i++) {
        uint32_t color = Wheel( ((i * 256 / strip.numPixels()) + j) % 256, brightness);
        strip.setPixelColor(i, color);
    }

    if (j < 256*5) { j++; } else {j = 0;}

    strip.show();
}

#ifdef TEST

int main()
{
    return 0;
}

#endif