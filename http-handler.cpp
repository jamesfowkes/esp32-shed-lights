#include <Arduino.h>
#include <string.h>

#include <HTTPClient.h>

#include <TaskAction.h>

#include "fixed-length-accumulator.h"

#include "http-handler.h"

HTTPHandler::HTTPHandler()
{
}

void HTTPHandler::download_state_printer()
{

    if (m_download_count)
    {
        Serial.print("Got ");
        Serial.print(m_download_count);
        Serial.print(" of ");
        Serial.print(m_download_size);
        Serial.println(" bytes.");
        Serial.flush();
    }
}

bool HTTPHandler::handle_get_stream(FixedLengthAccumulator& dst)
{
    static unsigned long last_run = 0;
    
    if (millis() == last_run) { return false; }
    if (!mp_http_client) { return false; }

    WiFiClient * stream = mp_http_client->getStreamPtr();

    if (stream)
    {
        if (!m_done)
        {
            if (m_download_size > 0)
            {
                size_t size = stream->available();
                while(size)
                {
                    dst.writeChar(stream->read());
                    size--;
                    m_download_count++;
                }
                Serial.println("Stream read end");
                Serial.print("Download count: ");
                Serial.println(m_download_count);
                Serial.print("Download size: ");
                Serial.println(m_download_size);
                Serial.flush();
                m_done = m_download_count == m_download_size;
            }
            else
            {
                size_t size = stream->available();
                while(size)
                {
                    dst.writeChar(stream->read());
                    size--;
                    m_download_count++;
                }
                Serial.println("Stream read end");
                Serial.print("Download count: ");
                Serial.println(m_download_count);
                m_done = true;
            }
        }
    }
    else
    {
        Serial.println("No HTTP stream");
        Serial.flush();
    }

    if (m_done)
    {
        m_download_count = 0;
        m_download_size = 0;
        Serial.println("Done.");
        Serial.flush();
        Serial.println("HTTP client end");
        Serial.flush();
        delete mp_http_client;
    }

    return m_done;
}

bool HTTPHandler::start_download(char const * const url)
{
    bool success = false;

    mp_http_client = new HTTPClient();

    mp_http_client->setReuse(true);
    mp_http_client->begin(url);
    int response = mp_http_client->GET();
    Serial.println("Starting download...");
    Serial.flush();
    switch(response)
    {
        case HTTP_CODE_OK:
        m_done = false;
            m_download_size = mp_http_client->getSize();
            if (m_download_size > 0)
            {
                m_download_count = 0;
                Serial.print("HTTP GET OK. Streaming ");
                Serial.print(m_download_size);
                Serial.println(" bytes...");
                Serial.flush();
            }
            else
            {
                m_download_count = 0;
                Serial.println("HTTP GET OK. No Content-Length provided.");
                Serial.flush();
            }
            success = true;
            break;
        default:
            Serial.print("Got HTTP response ");
            Serial.println(response);
            Serial.flush();
            break;
    }
    return success;
}

void HTTPHandler::loop()
{
    if (m_printer_next_tick == millis())
    {
        m_printer_next_tick = millis() + 100;
        download_state_printer();
    }
}
