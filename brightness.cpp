#include <stdint.h>

#include "brightness.h"

#define MINIMUM_BRIGHTNESS (8)
#define BRIGHTNESS_RANGE (255- MINIMUM_BRIGHTNESS)

static uint32_t seconds_into_current_day(uint32_t s)
{
    return s - ((s / 86400) * 86400);
}

uint16_t calculate_brightness(uint32_t now, uint32_t on_time, uint32_t off_time)
{
    uint16_t brightness = 0;
    uint32_t seconds_today = seconds_into_current_day(now);
    on_time = seconds_into_current_day(on_time);
    off_time = seconds_into_current_day(off_time);
    uint32_t total_on_time = off_time - on_time;
    uint32_t half_on_time = total_on_time / 2;
    uint32_t max_brightness_time = on_time + half_on_time;

    if ((seconds_today >= on_time) && (seconds_today < max_brightness_time))
    {
        brightness = ((seconds_today - on_time) * BRIGHTNESS_RANGE ) / half_on_time;
        brightness += MINIMUM_BRIGHTNESS;
    }
    else if ((seconds_today >= max_brightness_time ) && (seconds_today <= off_time))
    {
        brightness = ((off_time - seconds_today) * BRIGHTNESS_RANGE ) / half_on_time;
        brightness += MINIMUM_BRIGHTNESS;
    }
    return brightness;
}


#ifdef UNIT_TEST

#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

const uint16_t TEST_ON_TIME = HHMM_TO_SECS(17,00);
const uint16_t TEST_MID_TIME = HHMM_TO_SECS(17,30);
const uint16_t TEST_OFF_TIME = HHMM_TO_SECS(18,00);

class BrightnessTest : public CppUnit::TestFixture  {
    
    CPPUNIT_TEST_SUITE(BrightnessTest);

    CPPUNIT_TEST(test_brightness_starts_at_minimum);
    CPPUNIT_TEST(test_brightness_ends_at_minimum);
    CPPUNIT_TEST(test_brightness_is_maximum_at_midpoint);
    CPPUNIT_TEST(test_brightness_is_half_at_quarters);
    
    CPPUNIT_TEST_SUITE_END();

    void test_brightness_starts_at_minimum()
    {
        CPPUNIT_ASSERT_EQUAL((uint16_t)0, calculate_brightness(TEST_ON_TIME-1, TEST_ON_TIME, TEST_OFF_TIME));
        CPPUNIT_ASSERT_EQUAL((uint16_t)MINIMUM_BRIGHTNESS, calculate_brightness(TEST_ON_TIME, TEST_ON_TIME, TEST_OFF_TIME));
    }

    void test_brightness_ends_at_minimum()
    {
        CPPUNIT_ASSERT_EQUAL((uint16_t)MINIMUM_BRIGHTNESS, calculate_brightness(TEST_OFF_TIME, TEST_ON_TIME, TEST_OFF_TIME));
        CPPUNIT_ASSERT_EQUAL((uint16_t)0, calculate_brightness(TEST_OFF_TIME+1, TEST_ON_TIME, TEST_OFF_TIME));
    }

    void test_brightness_is_maximum_at_midpoint()
    {
        CPPUNIT_ASSERT_EQUAL((uint16_t)255, calculate_brightness(TEST_MID_TIME, TEST_ON_TIME, TEST_OFF_TIME));
    }

    void test_brightness_is_half_at_quarters()
    {
        CPPUNIT_ASSERT_EQUAL((uint16_t)((255 + MINIMUM_BRIGHTNESS)/2), calculate_brightness(HHMM_TO_SECS(17,15), TEST_ON_TIME, TEST_OFF_TIME));
        CPPUNIT_ASSERT_EQUAL((uint16_t)((255 + MINIMUM_BRIGHTNESS)/2), calculate_brightness(HHMM_TO_SECS(17,45), TEST_ON_TIME, TEST_OFF_TIME));
    }

};

int main()
{
   CppUnit::TextUi::TestRunner runner;

   CPPUNIT_TEST_SUITE_REGISTRATION( BrightnessTest );

   CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();

   runner.addTest( registry.makeTest() );
   runner.run();

   return 0;
}

#endif