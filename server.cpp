#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

#include "application.h"
#include "server.h"
#include "sun-times.h"
#include "watchdog.h"

static WebServer s_server(80);
static char const * const HTML_TEMPLATES[] = {\
"<html>\n"
"    <head>\n"
"        <title>ESP32 Shed Lights</title>\n"
"        <style>\n"
"           html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}\n"
"           button { border: none; color: white; padding: 16px 40px;"
" text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}\n"
"           button.red {background-color: #EE2222;}\n"
"           button.green {background-color: #22EE22;}\n"
"        </style>\n"
"    </head>\n"
"    <body>\n"
"        <h1>Shed Lights Status Board</h1>\n"
"        <h2>Uptime: "
, /*********/
"        </h2>\n"
"        <h2>Time now: "
, /*********/
"        </h2>\n"
"        <h2>Sunset time: "
, /*********/
"        </h2>\n"
"        <h2>Brightness: "
, /*********/
"        </h2>\n"
"        <p><a href=\"/restart\"><button class=\"button red\">Restart</button></a></p>"
"    </body>\n"
"</html>"
};

static char s_buffer[1024];

void render_page()
{
    char time_buffer[64] = "Unknown";
    char sunset_time_buffer[64] = "Unknown";
    uint8_t brightness = application_get_brightness();
    struct tm time;

    if (getLocalTime(&time, 0))
    {
        sprintf(time_buffer, "%s", asctime(&time));
    }

    if (suntime_get_sunset_time(time))
    {
        uint32_t sunset_time_seconds = suntime_get_sunset_timestamp();

        uint32_t len = strftime(sunset_time_buffer, 64, "%H:%M:%S", &time);
        sprintf(sunset_time_buffer + len, " (%u)", sunset_time_seconds);
    }

    sprintf(s_buffer, "%s%lu%s%s%s%s%s%u%s",
        HTML_TEMPLATES[0],
        millis(),
        HTML_TEMPLATES[1],
        time_buffer,
        HTML_TEMPLATES[2],
        sunset_time_buffer,
        HTML_TEMPLATES[3],
        brightness,
        HTML_TEMPLATES[4]
    );
}

void handle_index()
{
    render_page();
    s_server.sendHeader("Cache-Control", "no-cache");
    s_server.send(200, "text/html", s_buffer);
}

void handle_restart()
{
    watchdog_start();
    while(1);
}

void server_setup()
{
    s_server.on("/index", HTTP_GET, handle_index);
    s_server.on("/restart", HTTP_GET, handle_restart);
    s_server.begin();
}

void server_loop()
{
    s_server.handleClient();
}
