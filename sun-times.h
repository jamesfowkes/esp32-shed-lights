#ifndef _SUN_TIMES_H_
#define _SUN_TIMES_H_

void suntime_update();
uint32_t suntime_get_sunset_timestamp();
bool suntime_get_sunset_time(struct tm& time);

#endif
