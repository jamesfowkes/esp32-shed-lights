#ifndef _WATCHDOG_H_
#define _WATCHDOG_H_

void watchdog_start();
void watchdog_kick();

#endif
