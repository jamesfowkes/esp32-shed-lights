/* C/C++ Includes */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>

/* Arduino Includes */

#include <Arduino.h>
#include <ArduinoJson.h>
#include <TaskAction.h>
#include "fixed-length-accumulator.h"

/* Application Includes */

#include "http-handler.h"

/* Private Constants and Variables */

static const char URL[] = "http://api.sunrise-sunset.org/json?lat=52.083277&lng=0.182850";
static StaticJsonDocument<512> s_json_document;

static bool s_downloading = false;
static const uint16_t MAX_TEXT_SIZE = 512;
static char s_text_buffer[MAX_TEXT_SIZE];
static FixedLengthAccumulator s_text_accumulator(s_text_buffer, MAX_TEXT_SIZE);
static HTTPHandler s_http_handler;

static struct tm s_timeinfo;
static uint8_t s_last_date_updated = UINT8_MAX;

/* Private Functions */

static void update_task_fn(TaskAction* this_task)
{
    (void)this_task;
    if (getLocalTime(&s_timeinfo, 0))
    {
        if (s_last_date_updated != s_timeinfo.tm_mday)
        {
            if (!s_downloading)
            {
                Serial.print("Updating sunset time...");
                s_text_accumulator.reset();
                s_downloading = s_http_handler.start_download(URL);
            }
        }
    }
}
static TaskAction s_update_task(update_task_fn, 5000, INFINITE_TICKS);

static char * sanitize(FixedLengthAccumulator& accum)
{
    char * pStart = strchr(accum.c_str(), '{');
    char * pEnd = strrchr(accum.c_str(), '}');
    uint16_t length = accum.length();

    accum.remove(length - (pEnd - accum.c_str()) - 1);
    return pStart;
}
    
/* Public Functions */

void suntime_update()    
{
    s_update_task.tick();
}

uint32_t suntime_get_sunset_timestamp()
{
    static uint32_t hour, minute, second;
    uint32_t sunset_time = 0UL;
    if (s_downloading)
    {
        if (s_http_handler.handle_get_stream(s_text_accumulator))
        {
            s_downloading = false;

            char * pJson = sanitize(s_text_accumulator);
            DeserializationError err = deserializeJson(s_json_document, pJson);

            if (err == DeserializationError::Ok)
            {
                const char* sunset_str = s_json_document["results"]["sunset"];

                Serial.print("Got sunset time:");
                Serial.println(sunset_str);

                sscanf(sunset_str, "%d:%d:%d", &hour, &minute, &second);
                hour += 12; // Stupid API is in 12-hour clock;

                Serial.print("Parsed: ");
                Serial.print(hour); Serial.print(":");
                Serial.print(minute); Serial.print(":");
                Serial.println(second);

                s_last_date_updated = s_timeinfo.tm_mday;
                s_downloading = false;
            }
            else
            {
                Serial.print("Could not parse sunset time (err");
                Serial.print(err.c_str());
                Serial.println(")");
            }
        }
    }

    if (s_last_date_updated == s_timeinfo.tm_mday)
    {
        time_t now = time(nullptr);
        now /= 86400;
        now *= 86400;
        sunset_time = now + (hour * 60 * 60) + (minute * 60) + second;
        Serial.print("Sunset timestamp UTC: ");
        Serial.println(sunset_time);

    }
    return sunset_time;
}

bool suntime_get_sunset_time(struct tm& time)
{
    time_t sunset_timestamp = (time_t)suntime_get_sunset_timestamp();

    if (sunset_timestamp)
    {
        localtime_r(&sunset_timestamp, &time);
    }

    return (bool)sunset_timestamp;
}
