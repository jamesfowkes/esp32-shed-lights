#ifndef _APPLICATION_H_
#define _APPLICATION_H_

uint8_t application_get_brightness();
void application_setup();
void application_loop();

#endif
